import {Given, When, Then} from "cypress-cucumber-preprocessor/steps";
// Navigate to main page url
Given(/^I visit taalimi home page$/,  ()=>{
    cy.visit('https://www.ta3limy.com')
});
// dynamic step to click on the button/links
Given(/^I click the "([^"]*)" (button|link)$/,(element,type) =>{
    if (type === 'button') cy.get(`[id='${element}']`).click()
    if (type === 'link') cy.xpath(`//*[text()='${element}']`).first().click()
});

// add wait time
Given(/^wait "([^"]*)" seconds$/,  (seconds)=>{
    cy.wait(seconds*1000)
});

// fill in the fields with given value
When(/^I fill in the "([^"]*)" with "([^"]*)"$/, function (field, value) {
    cy.get(`[id='${field}']`).type(value)
});

// assert the texts on the page
When(/^I (should|should not) see "([^"]*)"$/, function (vision, value) {
    if (vision ==='should') cy.contains(value).should('exist')
    if (vision ==='should not') cy.contains(value).should('not.exist')
});
// fill in the fields with random data
Given(/^I fill in the "([^"]*)" field with random data$/, function (field) {
    var value = (field === 'firstName' || 'lastName') ? cy.faker.lorem.words() : cy.faker.phone.phoneNumber() ;
    cy.get(`[id='${field}']`).type(value)
});

// select an option from  a drop down list
Given(/^I select "([^"]*)" from "([^"]*)" dropdown list$/, function (option, dropdownlist) {
cy.get(`[id='${dropdownlist}']`).select(option)
});

// dynamic step for checkboxes
Given(/^I check "([^"]*)"$/, function (field) {
// `//*[text()='${element}']`
    ////*[@id="termsAndConditionsCheck"]
    cy.get(`[id='${field}']`).click({force: true})
});