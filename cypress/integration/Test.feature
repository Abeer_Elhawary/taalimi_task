Feature:  Test Common Q&A page

  Scenario:  Assert the fields of FQA page
    Given I visit taalimi home page
    And wait "3" seconds
    And I click the "help-menu" button
    And I click the "أسئلة شائعة" link
    Then I should see "أسئلة شائعة"
    And I should see "ما هي المؤسسة ؟"
    And I should see "ما هو تعليمي ؟"
    And I should see "من المستفيد من تعليمي ؟"
    And I should see "ما هو المحتوي الموجود في تعليمي؟"
    And I should see "هل تعليمي مجاني ؟"
    And I should see "كيف أقوم باستخدام تعليمي ؟"
    And I should see "كيف يتم احتساب النقاط ؟"

  Scenario: Test the happy scenario of student registeration
    Given I visit taalimi home page
    And wait "3" seconds
    And I click the "للطلبة" link
    And wait "3" seconds
    And I click the "تسجيل حساب" link
    And wait "4" seconds
    And I click the "طالب" link
    And I fill in the "firstName" field with random data
    And I fill in the "lastName" field with random data
    And I fill in the "mobileNumber" with "01055536549"
    And I click the "ذكر" link
    And I select "الصف الأول - الاعدادي" from "grade" dropdown list
    And I fill in the "password" with "Test_1234"
    And I fill in the "passwordConfirmation" with "Test_1234"
    And I check "termsAndConditionsCheck"
    And I click the "تسجيل حساب" link
    And wait "3" seconds

