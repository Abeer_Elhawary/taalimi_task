describe("Test Student Registeration", function () {

    // Load the data from  Fixtures/student.json
    beforeEach("Load", function ()
    {
        cy.fixture("student").then((data)=>
        {
            this.key = data
        })

    })
   // insert valid data
    it("Insert valid data to Student role", function()
    {
        cy.visit('https://www.ta3limy.com');
        cy.xpath("//*[text()='تسجيل حساب']").first().click()

        // choose the role to be a student
        cy.xpath(`//*[text()='${this.key.role}']`).first().click()
        cy.get("[id='firstName']").type(this.key.firstname)
        cy.get("[id='lastName']").type(this.key.lastname)
        cy.get("[id='mobileNumber']").type(this.key.validmobilenumber)
        cy.get("[id='password']").type(this.key.validpassword)
        cy.get("[id='passwordConfirmation']").type(this.key.confirmpassword)
        // choose valid grade
        cy.get("[id='grade']").select(this.key.grade_1)
        // choose male gender
        cy.xpath(`//*[text()='${this.key.maleoption}']`).first().click()
        // check terms and conditions
        cy.get("[id='termsAndConditionsCheck']").click({force: true})
        cy.xpath("//*[text()='تسجيل حساب']").first().click()
        cy.contains('رمز التحقق مطلوب').should('exist')
    })
        // assert invalid password message
    it("Insert invalid password and assert the error message of invalid password", function()
    {
        cy.visit('https://www.ta3limy.com');
        cy.xpath("//*[text()='تسجيل حساب']").first().click()

        // choose the role to be a student
        cy.xpath(`//*[text()='${this.key.role}']`).first().click()
        cy.get("[id='firstName']").type(this.key.firstname)
        cy.get("[id='lastName']").type(this.key.lastname)
        cy.get("[id='mobileNumber']").type(this.key.validmobilenumber)
        // enter invalid password
        cy.get("[id='password']").type(this.key.invalidpassword)
        // choose valid grade
        cy.get("[id='grade']").select(this.key.grade_1)
        // choose male gender
        cy.xpath(`//*[text()='${this.key.maleoption}']`).first().click()
        // check terms and conditions
        cy.get("[id='termsAndConditionsCheck']").click({force: true})
        cy.xpath("//*[text()='تسجيل حساب']").first().click()
        cy.contains('رمز التحقق مطلوب').should('exist')
        cy.contains('كلمة السر يجب أن تحتوي على 8 أحرف أو أكثر باستعمال مزيج من الأحرف والأرقام').should('exist')
    })

    // assert the Questions list
    it("Assertion of the Questions on FQA page ", function()
    {
        cy.visit('https://www.ta3limy.com');
        cy.get("[id='help-menu']").click()
        cy.xpath("//*[text()='أسئلة شائعة']").first().click()
        cy.contains('أسئلة شائعة').should('exist')
        cy.contains('ما هي المؤسسة ؟').should('exist')
        cy.contains('ما هو تعليمي ؟').should('exist')
        cy.contains('من المستفيد من تعليمي ؟').should('exist')
        cy.contains('هل تعليمي مجاني ؟').should('exist')
        cy.contains('كيف أقوم باستخدام تعليمي ؟').should('exist')
        cy.contains('كيف يتم احتساب النقاط ؟').should('exist')
    })

    // Check the answers on FQA
    it("open the desc of Q2 and verify it's published correctly", function()
        {
            cy.visit('https://www.ta3limy.com');
            cy.get("[id='help-menu']").click()
            cy.xpath("//*[text()='أسئلة شائعة']").first().click()
            cy.wait(1000);
            cy.xpath("//text()[contains(.,'ما هو تعليمي ؟')]/ancestor::p[1]").first().click()
            // assert the Answer is published correctly
            cy.contains(this.key.desc_2).should('exist')
        })

    }
)
